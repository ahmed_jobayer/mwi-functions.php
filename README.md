# Description

When integrating Magento and WordPress, there's a function that they both share which conflicts. Fortunately, that function has been deprecated in Magento since version 1.3. We can safely avoid using that function by replacing a single core file in Magento. We will replace it in our **local** code pool, so that it is not affected by upgrades.

#Installation

Simply download the functions.php provided in this repo and use the folder structure provided. It should be placed in *~/your-magento/app/code/local/Mage/Core/functions.php*. If the folders do not exist, you can create them.

#Compatibility

This functions.php has been tested with version 1.6.0.0 to 1.9.1.0, and can be used for all.